const express = require('express')
const app = express()
const port = 1770
const bodyParser = require('body-parser')
const cors = require('cors');
const jwt = require('jsonwebtoken');
const jsonwebtoken = require('jsonwebtoken');

const secret = 'S3cr31'

app.use(cors());
app.use(bodyParser.json());

let users = [
  {
    name: "strackz",
    password: "123"
  },
  {
    name: "nellita",
    password: "123"
  }
]

app.get('/', (req, res) => {
  res.send('Hello World AUTH-SERVER!')
})

app.listen(port, () => {
  console.log(`[AUTH-SERVER] Example app listening on port ${port}`)
})

app.post('/authorize', (req, res) => {
  let user = users.find(user => (req.body.name === user.name && req.body.password === user.password))

  if (user === undefined) res.send('UnAuthorized AUTH').status(403);
  else res.json({
    "token": jsonwebtoken.sign(user, secret)
  }).status(200)
  
})

app.post('/auth/verify', (req, res) => {
  let token = (req.body.authorization !== undefined) ? req.body.authorization.split(" ")[1] : undefined;
  if (token === undefined) res.send("nop").status(403)
  else {
    if(jsonwebtoken.verify(token, secret) !== undefined) res.send('ok').status(200)
    else res.send('fuck u').status(403)
  } 
})